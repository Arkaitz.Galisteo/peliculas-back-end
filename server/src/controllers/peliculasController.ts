import { json, Request, Response } from 'express';

import db from '../database';

class PeliculasController {

    //Muestra todos las películas
    public async getPeliculas(req: Request, res: Response): Promise<void> {

        try {
            const peliculas = await db.query(`select * from peliculas`);
            console.log(peliculas);
            
            res.json(peliculas)
        } catch (error) {
            res.json(error)
            console.log(error)
        }


    }
    //Muestra una película
    public async getPelicula(req: Request, res: Response): Promise<any> {
        const { id } = req.params;
        const pelicula = await db.query(`select * from peliculas where imdbID = '${id}' `)
        console.log(pelicula)
        return res.json(pelicula)
    }
    //Añade una película a la base de datos
    public async createPelicula(req: Request, res: Response): Promise<void> {
        console.log(req.body)
        

        try {
            await db.query(`INSERT INTO peliculas (imdbID, Title, Year, Assessment) VALUES ("${req.body['imdbID']}" , "${req.body['Title']}", "${req.body['Year']}", ${req.body['Assessment']})`)
            res.json({ message: 'The film was created' })
        } catch (error) {
            console.log(error)
        }
    }

    //Actualiza una pelicula
    public async updatePelicula(req: Request, res: Response): Promise<void> {
        const { id } = req.params;

        console.log(req.body)


        try {
            console.log('entra')
            await db.query(`UPDATE peliculas SET  Assessment = '${req.body['Assessment']}'  WHERE imdbID = '${id}'`)
            res.json({ message: 'The film has been updated' })

        } catch (error) {
            console.log(error)
        }
    }

}

export const peliculasController = new PeliculasController();

