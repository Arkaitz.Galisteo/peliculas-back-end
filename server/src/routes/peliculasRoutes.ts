import { Router } from 'express';
import {peliculasController} from '../controllers/peliculasController';

class PeliculasRoutes{
    public router: Router = Router();

    constructor(){
        this.config();
    }
    config(): void{
        this.router.get('/', peliculasController.getPeliculas)
        this.router.get('/:id', peliculasController.getPelicula)
        this.router.post('/', peliculasController.createPelicula)
        this.router.put('/:id', peliculasController.updatePelicula)
    }

}

const peliculasRoutes = new PeliculasRoutes();
export default peliculasRoutes.router;