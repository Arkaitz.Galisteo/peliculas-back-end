"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.peliculasController = void 0;
const database_1 = __importDefault(require("../database"));
class PeliculasController {
    //Muestra todos las películas
    getPeliculas(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const peliculas = yield database_1.default.query(`select * from peliculas`);
                console.log(peliculas);
                res.json(peliculas);
            }
            catch (error) {
                res.json(error);
                console.log(error);
            }
        });
    }
    //Muestra una película
    getPelicula(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            const pelicula = yield database_1.default.query(`select * from peliculas where imdbID = '${id}' `);
            console.log(pelicula);
            return res.json(pelicula);
        });
    }
    //Añade una película a la base de datos
    createPelicula(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            console.log(req.body);
            try {
                yield database_1.default.query(`INSERT INTO peliculas (imdbID, Title, Year, Assessment) VALUES ("${req.body['imdbID']}" , "${req.body['Title']}", "${req.body['Year']}", ${req.body['Assessment']})`);
                res.json({ message: 'The film was created' });
            }
            catch (error) {
                console.log(error);
            }
        });
    }
    //Actualiza una pelicula
    updatePelicula(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { id } = req.params;
            console.log(req.body);
            try {
                console.log('entra');
                yield database_1.default.query(`UPDATE peliculas SET  Assessment = '${req.body['Assessment']}'  WHERE imdbID = '${id}'`);
                res.json({ message: 'The film has been updated' });
            }
            catch (error) {
                console.log(error);
            }
        });
    }
}
exports.peliculasController = new PeliculasController();
