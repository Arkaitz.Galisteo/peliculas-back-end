"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const peliculasController_1 = require("../controllers/peliculasController");
class PeliculasRoutes {
    constructor() {
        this.router = (0, express_1.Router)();
        this.config();
    }
    config() {
        this.router.get('/', peliculasController_1.peliculasController.getPeliculas);
        this.router.get('/:id', peliculasController_1.peliculasController.getPelicula);
        this.router.post('/', peliculasController_1.peliculasController.createPelicula);
        this.router.put('/:id', peliculasController_1.peliculasController.updatePelicula);
    }
}
const peliculasRoutes = new PeliculasRoutes();
exports.default = peliculasRoutes.router;
